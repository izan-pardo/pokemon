/*Obtener todos los pkmn*/
  SELECT pokemon.nombre,pokemon.sexo,tipo.descripcion,naturaleza.descripcion,habilidad.descripcion
    FROM pokemon,pokemon_tipo,tipo,naturaleza,habilidad
    WHERE pokemon.nombre=pokemon_tipo.nombre
    AND pokemon_tipo.id_tipo=tipo.id_tipo
    AND pokemon.naturaleza=naturaleza.id_nat
    AND pokemon.habilidad=habilidad.id_hab;


/*obtener todos los pokemon con mas informacion adicional */
  SELECT pokemon.nombre,pokemon.sexo,GROUP_CONCAT(DISTINCT tipo.descripcion),naturaleza.descripcion,habilidad.descripcion
    FROM pokemon,pokemon_tipo,tipo,naturaleza,habilidad,debilidad
    WHERE pokemon.nombre=pokemon_tipo.nombre
    AND pokemon_tipo.id_tipo=tipo.id_tipo
    AND pokemon.naturaleza=naturaleza.id_nat
    AND pokemon.habilidad=habilidad.id_hab
    GROUP BY pokemon.nombre;


create view debilidades as select * from (select debilidad.id_debilidad,tipo.descripcion
from tipo,debilidad
where tipo.id_tipo=debilidad.id_debilidad) as debilidades;


CREATE VIEW debilidades2 as select tipo.descripcion as origen,GROUP_CONCAT(debilidades.descripcion) as vulnerable
from tipo,debilidad,debilidades
where tipo.id_tipo=debilidad.id_tipo
and debilidad.id_debilidad=debilidades.id_debilidad
GROUP BY tipo.descripcion;


/*TODO JUNTO */
  SELECT pokemon.nombre,pokemon.sexo,GROUP_CONCAT(DISTINCT tipo.descripcion) as TIPO,naturaleza.descripcion AS NATURALEZA,habilidad.descripcion AS HABILIDAD,group_concat(distinct debilidades2.vulnerable) AS VULNERABLE
    FROM pokemon,pokemon_tipo,tipo,naturaleza,habilidad,debilidad,debilidades2
    WHERE pokemon.nombre=pokemon_tipo.nombre
    AND pokemon_tipo.id_tipo=tipo.id_tipo
    AND pokemon.naturaleza=naturaleza.id_nat
    AND pokemon.habilidad=habilidad.id_hab
    AND tipo.descripcion=debilidades2.origen
    GROUP BY pokemon.nombre;


