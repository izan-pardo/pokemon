<?php
class DB{
    public static function conectar()
    {
        $opc = array(
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ); //para activar la captura de errores
        $usuario = "root";
        $contrasena = "";
    
        $dsn = "mysql:host=localhost;dbname=pkmn";
        $conexion = new PDO($dsn, $usuario, $contrasena, $opc);
        return $conexion;
    }

    public static function obtenerPokemons()
    {
        $conexion = self::conectar();
        if ($conexion == null) {
            echo "Error al conectarse a la base de datos";
            exit();
        }
        $sql = "  SELECT pokemon.nombre,pokemon.sexo,GROUP_CONCAT(DISTINCT tipo.descripcion) as TIPO,naturaleza.descripcion AS NATURALEZA,habilidad.descripcion AS HABILIDAD,group_concat(distinct debilidades2.vulnerable) AS VULNERABLE
        FROM pokemon,pokemon_tipo,tipo,naturaleza,habilidad,debilidad,debilidades2
        WHERE pokemon.nombre=pokemon_tipo.nombre
        AND pokemon_tipo.id_tipo=tipo.id_tipo
        AND pokemon.naturaleza=naturaleza.id_nat
        AND pokemon.habilidad=habilidad.id_hab
        AND tipo.descripcion=debilidades2.origen
        GROUP BY pokemon.nombre
		ORDER BY num_pokedex;";
        $resultado = $conexion->query($sql);
        $datos = array();
        while ($fila = $resultado->fetch()) {
            array_push($datos, $fila); //añado al array ciclo un objeto ciclo con todos los datos que salen de la base de datos
        }
        return $datos;
    }
	
	 public static function infoPokemon($nombre)
    {
        $conexion = self::conectar();
        if ($conexion == null) {
            echo "Error al conectarse a la base de datos";
            exit();
        }
        $sql = "  SELECT pokemon.nombre,pokemon.sexo,GROUP_CONCAT(DISTINCT tipo.descripcion) as TIPO,naturaleza.descripcion AS NATURALEZA,habilidad.descripcion AS HABILIDAD,group_concat(distinct debilidades2.vulnerable) AS VULNERABLE
        FROM pokemon,pokemon_tipo,tipo,naturaleza,habilidad,debilidad,debilidades2
        WHERE pokemon.nombre=pokemon_tipo.nombre
        AND pokemon_tipo.id_tipo=tipo.id_tipo
        AND pokemon.naturaleza=naturaleza.id_nat
        AND pokemon.habilidad=habilidad.id_hab
        AND tipo.descripcion=debilidades2.origen
		AND pokemon.nombre='".$nombre."' 
        GROUP BY pokemon.nombre
		ORDER BY num_pokedex;";
	
        $resultado = $conexion->query($sql);
     
       $fila = $resultado->fetch();
        return $fila;
    }
	
	
	

}


?>