<?php
/* @var $this yii\web\View */

$this->title = 'Mundo Pokemon';
include_once "../config/base.php";
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Pokemon</h1>


    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-12">
                <?php
                if (isset($_REQUEST['Buscar'])) {
                    $datos = DB::filtroPokemon($_REQUEST['tipo'], $_REQUEST['habilidad']);


                    foreach ($datos as $valor) {
                        echo "<div class='pok' id='" . $valor['nombre'] . "'>";
                        echo "<a href='index.php?r=sitio%2Fpokemon_info&nombre=fuego&nombre=" . $valor['nombre'] . "'><img src='imagen/" . $valor['nombre'] . ".png'/></a>";
                        echo "<p>" . $valor['nombre'] . "</p>";

                        echo "</div>";
                    }
                } else {
                    ?>
                    <h2>Bienvenidos a Mundo Pokemon</h2>

                    <p>Esta aplicación recoge la información de los pokemons. ¡Usa la búsqueda avanzada para encontrar Pokemon
                    por su tipo o habilidad! 
                        <p>Por el momento dispone de los Pokemons de la 1ª generación, estamos en proceso de implementar las
                        demás generaciones y futuras funciones como la Crianza Pokemon. ¡No te lo pierdas!
                        </p>
                        </p>
                    <?php
                }
                ?>

            </div>

        </div>

    </div>
</div>
